/**
 * 
 */
package com.java.exercises.exer2;

/**
 * @author b.orobia
 *
 */
public class Producer extends Thread {

	StringBuffer strBuffer;

	Producer() {
		strBuffer = new StringBuffer(10);
	}

	public void run() {

		// Add values in the buffer
		for (int i = 0; i < 10; i++) {
			try {
				strBuffer.append(i);
				System.out.println("Number Produced: " + i);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("Buffer bag is full!");

		Consumer consumerProd = new Consumer(this);
	}

}

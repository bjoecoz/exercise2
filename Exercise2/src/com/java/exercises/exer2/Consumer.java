/**
 * 
 */
package com.java.exercises.exer2;

/**
 * @author b.orobia
 *
 */
public class Consumer extends Thread {

	Producer producer;
	Thread thread;

	Consumer(Producer prodTemp) {
		producer = prodTemp;

		// New object thread
		thread = new Thread(this);
		thread.start();
	}

	public void run() {
		System.out.println();
		try {


			for (int i = 0; i < 10; i++) {
				System.out.println("Number Consumed: "+ producer.strBuffer.charAt(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Buffer bag is now Empty!");
	}
}

/**
 * 
 */
package com.java.exercises.exer2;

/**
 * @author b.orobia
 *
 */
public class Main {

	public static void main(String[] args) {

		Producer mainProd = new Producer();
		Thread mainThread = new Thread(mainProd);

		mainThread.start();
	}

}
